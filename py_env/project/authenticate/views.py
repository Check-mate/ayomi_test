from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib import messages
from .forms import RegistrationForm, ModificationProfileForm

def home(request):
    return render(request, 'authenticate/home.html', {})


def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'You have successfully been logged in !')
            return redirect('home')
        else:
            messages.success(request, 'Login Error: Retry...')
            return redirect('login')
    else:
        return render(request, 'authenticate/login.html', {})

def logout_user(request):
    logout(request)
    messages.success(request, 'You have been logged out !')
    return redirect('home')

def register_user(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, 'You have been Registered into our system !')
            return redirect('home')
    else:
        form = RegistrationForm()
    context = {'form': form}
    return render(request, 'authenticate/register.html', context)

def modif_profile(request):
    if request.method == 'POST':
        form = ModificationProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'You have been edited your profile infos !')
            return redirect('home')
    else:
        form = ModificationProfileForm(instance=request.user)
    context = {'form': form}
    return render(request, 'authenticate/edit.html', context)
